import logoMobile from "./logoMobile.svg";
import "./App.css";
// import useWindowDimensions from "./Utils/WindowSize";
// import { Routes, Route } from "react-router-dom";
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import Navbar from "./Components/Navbar/Navbar"
import Home from "./Components/Home/Home"


function App() {
  // const { width } = useWindowDimensions();
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home/>} />
      </Routes>
    </BrowserRouter>

    // <div className="App">
    //   {width > 768 ? (
    //     <img src={logoMobile} alt="" />
    //   ) : (
    //     <img src={logoMobile} className="iconMobile" alt="" />
    //   )}
    // </div>
  );
}

export default App;
