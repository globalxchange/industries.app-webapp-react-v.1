import React, { useState, useEffect } from 'react';
import './Home.css';
import Icon from "../../images/favicon-32x32.png"
import MainImage from "../../images/Rectangle.png"
import Teams from "../../images/teams.png";
import Players from "../../images/players.png";
import SmallIndus from "../../images/industat.png";
import UserImg from "../../images/Vector.png";
import MobileImage from "../../images/mobileimg.png";
import MainLogo from "../../images/mainlogo.png";
import LoginIcon from "../../images/loginicon.png";
import RegisterIcon from "../../images/registericon.png";
import AppleIcon from "../../images/appleicon.png";
import AndroidIcon from "../../images/androidicon.png";
import IndustriesIcon from "../../images/industries-icon.png";
import TeamForceIcon from "../../images/teamforce-icon.png";
import ProjectStreamIcon from "../../images/projectstream-icon.png";
import SnapayIcon from "../../images/snapay-icon.png";
import HolidaysIcon from "../../images/holidays-icon.png";
import IndianMarketIcon from "../../images/indianmarket-icon.png";


function Home() {
    // console.log(React.version);
    const [buttontext, setbuttontext] = useState("Start A Team")
    const [appcard, setappcard] = useState("none")
    const [uparrow, setuparrow] = useState("none")
    const [downarrow, setdownarrow] = useState("block")
    const [fullmask, setfullmask] = useState("")
    const [statsmask, setstatsmask] = useState("")
    const [hrmask, sethrmask] = useState("")


    const openapps = (e) => {
        if (appcard === "none") {
            setappcard("block")
            setuparrow("block")
            setdownarrow("none")
            setfullmask("page-mask")
            setstatsmask("stats-mask")
            sethrmask("hr-stats-mask")
        } else {
            setappcard("none")
            setuparrow("none")
            setdownarrow("block")
            setfullmask("")
            setstatsmask("")
            sethrmask("")
        }
    }

    return (
        <div style={{ overflow: "hidden" }} className={fullmask}>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid padd-cont-cust">
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <a className="navbar-brand " rel="noopener noreferrer" href="/#">
                        <div className="img-mbl">
                            <img src={MainLogo} alt="The icon" />&nbsp;
                        </div>
                        {/* <p className="homename">INDUSTRIES</p> */}
                    </a>
                    <div className="tog-n-cols">
                        {/* <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button> */}
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <a className="nav-link navsty active" rel="noopener noreferrer"
                                        aria-current="page" href="/#">Home</a>
                                    <p className="arrow-down"></p>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link navsty" rel="noopener noreferrer"
                                        href="/#">Players</a>
                                    <p className="arrow-down p1"></p>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link navsty" rel="noopener noreferrer"
                                        href="/#">Teams</a>
                                    <p className="arrow-down p2"></p>
                                </li>
                                <li className="nav-item">
                                    <a href="/#" className="nav-link disabled">Industries</a>
                                    <p className="arrow-down disabled p3"></p>
                                </li>
                                <li className="nav-item">
                                    <a href="/#" className="nav-link disabled">Owners</a>
                                    <p className="arrow-down disabled p4"></p>
                                </li>
                                <li className="nav-item">
                                    <a href="/#" className="nav-link disabled">Other</a>
                                    <p className="arrow-down disabled p5"></p>
                                </li>
                                <li className="nav-item" onClick={openapps}>
                                    <a className="nav-link navsty" rel="noopener noreferrer"
                                        href="/#">Apps</a>
                                    <p className="arrow-down p6" style={{ display: downarrow }}></p>
                                    <p className="arrow-up p6" style={{ display: uparrow }}></p>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </nav>
            <div className="apps-dropdown" style={{ display: appcard }}>
                <div className="row">
                    <div className="col-sm-3 the-left-width">
                        <div className="apps-left_data log-1">
                            <img src={LoginIcon} alt="login-icon" />
                        &nbsp;&nbsp;&nbsp;
                        <span className="left-text">Login</span>
                        </div>
                        <div className="apps-left_data log-1">
                            <img src={RegisterIcon} alt="login-icon" />
                        &nbsp;&nbsp;&nbsp;
                        <span className="left-text">Register</span>
                        </div>
                        <div className="apps-left_data log-1">
                            <img src={AppleIcon} alt="login-icon" />
                        &nbsp;&nbsp;&nbsp;
                        <span className="left-text">For Iphone</span>
                        </div>
                        <div className="apps-left_data log-1">
                            <img src={AndroidIcon} alt="login-icon" />
                        &nbsp;&nbsp;&nbsp;
                        <span className="left-text">For Android</span>
                        </div>
                    </div>
                    <div className="col-sm-1 vertical-width">
                        <div className="vl"></div>
                    </div>
                    <div className="col-sm-8 right-width">
                        <p className="apps-right_title">One Account. Mutiple Advantages</p>
                        <p className="apps-right_desc">With An Industries Account You Also Get Access To The Employment Group Apps</p>
                        <br />
                        <div className="row">
                            <div className="col-sm-4 the-len">
                                <br />
                                <img src={IndustriesIcon} alt="Industries" className="img-icon" />
                                <br />
                                <br />
                                <br />
                                <br />
                                <img src={SnapayIcon} alt="Snapay" className="img-icon" />
                            </div>
                            <div className="col-sm-4 the-center">
                                <br />
                                <img src={TeamForceIcon} alt="Teamforce" className="img-icon" />
                                <br />
                                <br />
                                <br />
                                <br />
                                <img src={HolidaysIcon} alt="Holidays" className="img-icon" />
                            </div>
                            <div className="col-sm-4 the-right">
                                <br />
                                <img src={ProjectStreamIcon} alt="ProjectStream" className="img-icon" />
                                <br />
                                <br />
                                <br />
                                <br />
                                <img src={IndianMarketIcon} alt="IndianMarket" className="img-icon" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container1">
                <div className="metaindusimg">
                    <img src={MainImage} alt="mainimage" className="mainimg" />
                    <div className="row">
                        <div className="col-sm-5"></div>
                        <div className="col-sm-7">
                            <p className="image-title-text">You Have Entered The</p>
                            <p className="image-main-text">Meta-Industrial Revolution</p>
                            <p className="image-main-mobile-text">Meta-Industrial</p>
                            <p className="image-main-mobile-text1">Revolution</p>
                            {/* <div className="row">
                        <div className="col-sm-4"> */}
                            <div>
                                <input type="button" value="Start A Team" className="start-team" />
                                {/* </div> */}
                                {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
                                {/* <div className="col-sm-4">  */}
                                <input type="button" value="Be A Player" className="be-player" />
                            </div>
                            <div>
                                <input type="button" value="Teams" className="start-team-mobile" />
                                <input type="button" value="Players" className="be-player-mobile" />
                            </div>
                            {/* </div>
                        </div>  */}
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-6">
                    <div className={"stats" + " " + statsmask} >
                        <p className="stats-text">Stats Across All Industries</p>
                        <hr style={{ color: "#E7E7E7" }} className={hrmask} />
                        <div className="tableflex">
                            <img src={SmallIndus} alt="industries" className="tableimg" />
                            &nbsp;&nbsp;
                            <span className="stats-text">Industries</span>
                            <p className="stats-numtext">1</p>
                        </div>
                        <hr className={"hr-tag" + " " + hrmask} />
                        <div className="tableflex">
                            <img src={Teams} alt="teamos" className="tableimg" />
                            &nbsp;&nbsp;
                            <span className="stats-text">Teams</span>
                            <p className="stats-numtext">1</p>
                        </div>
                        <hr className={"hr-tag" + " " + hrmask} />
                        <div className="tableflex">
                            <img src={Players} alt="players" className="tableimg" />
                            &nbsp;&nbsp;
                            <span className="stats-text">Players</span>
                            <p className="stats-numtext">25</p>
                        </div>
                        <hr className={"hr-tag" + " " + hrmask} />
                    </div>
                </div>
                <div className="col-lg-6">
                    <div className={"stats stats-mobile" + " " + statsmask} >
                        <p className="stats-text">Top Performing Players</p>
                        <hr style={{ color: "#E7E7E7" }} className={hrmask} />
                        {/* <div className="tableflex">
                            <img src={UserImg} alt="userimg" className="tableimg" />
                            &nbsp;&nbsp;
                            <span className="stats-text">Shorupan P</span>
                            <p>@Shorupan</p>
                            <p className="stats-numtext">3 Teams</p>
                        </div> */}
                        <div className="titleimg-sty">
                            <img src={UserImg} alt="userimg" className="img-sty img-dimen" />
                            <div className="player-text">
                                <p className="title-sty title-adj">Shorupan P</p><p className="balance-sty">3 Teams</p>
                                <p className="email-adj">@shorupan</p><p className="balance-titlesty">Franchise Player</p>
                            </div>
                        </div>
                        <hr className={"hr-tag" + " " + hrmask} />
                        <div className="titleimg-sty">
                            <img src={UserImg} alt="userimg" className="img-sty img-dimen" />
                            <div className="player-text">
                                <p className="title-sty title-adj">Shorupan P</p><p className="balance-sty">3 Teams</p>
                                <p className="email-adj">@shorupan</p><p className="balance-titlesty">Franchise Player</p>
                            </div>
                        </div>
                        <hr className={"hr-tag" + " " + hrmask} />
                        <div className="titleimg-sty">
                            <img src={UserImg} alt="userimg" className="img-sty img-dimen" />
                            <div className="player-text">
                                <p className="title-sty title-adj">Shorupan P</p><p className="balance-sty">3 Teams</p>
                                <p className="email-adj">@shorupan</p><p className="balance-titlesty">Franchise Player</p>
                            </div>
                        </div>
                        <hr className={"hr-tag" + " " + hrmask} />
                    </div>
                </div>
            </div>
        </div >
    )
}
export default Home
